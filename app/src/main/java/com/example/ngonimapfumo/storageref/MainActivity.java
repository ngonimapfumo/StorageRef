package com.example.ngonimapfumo.storageref;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private EditText mPasswordField, mEmailField;
    private Button mCreateButton, mLoginButton;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCreateButton= findViewById(R.id.create_button);
        mLoginButton=findViewById(R.id.login_button);
        mEmailField = findViewById(R.id.emailEt);
        mPasswordField = findViewById(R.id.passEt);
        mAuth = FirebaseAuth.getInstance();
        mCreateButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);

    }

    private void sendEmailVerification(){
        user.sendEmailVerification().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
           if(task.isSuccessful()){
               Toast.makeText(MainActivity.this, "Check Email for verification",
                       Toast.LENGTH_SHORT).show();
           }

            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this,e.getLocalizedMessage()
                        , Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void signOut(){
        mAuth.signOut();
    }
    private boolean checkVerification(){
        if(user.isEmailVerified()){
            
        }

        return true;
    }

    private void verifyEmail(){

    }


    private void createUserWithEmailAndPassword(){

        String email= mEmailField.getText().toString();
        String password= mPasswordField.getText().toString();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            sendEmailVerification();
                            Toast.makeText(MainActivity.this, "user account created",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(MainActivity.this,task
                                            .getException()
                                            .getLocalizedMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    private void signInWithEmailAndPassword(){
        String email= mEmailField.getText().toString();
        String password= mPasswordField.getText().toString();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG,"Sign in successful");

                        }else{
                            Toast.makeText(MainActivity.this, task
                                    .getException().
                                    getLocalizedMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }

                });

        }


    @Override
    public void onClick(View view) {
        int position = view.getId();

        switch(position){
            case R.id.create_button:
                createUserWithEmailAndPassword();
                break;

            case R.id.login_button:
                signInWithEmailAndPassword();
                break;
        }

    }
}

